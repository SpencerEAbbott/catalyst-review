# Spartan Code Review #
This is a stripped-down code base of a simple site I recently made. 

## Main Templates: ##
wp-content -> themes -> catalyst-timber-theme -> templates

## Stylesheets and Scripts: ##
wp-content -> themes -> catalyst-timber-theme -> assets


#Where to view site:#
catalyst-sports-portfolio.spencerabbott.me

