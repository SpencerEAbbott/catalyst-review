
jQuery(document).ready(function($) {


  // Initialize WOW.js lazy loader vendor
    wow = new WOW(
      {
        boxClass:     'wow',      // default
        animateClass: 'animated', // default
        offset:       0,          // default
        mobile:       false,
        live:         true        // default
      }
    )

    //initialize wow
    wow.init();

  // smooth scrolll to IDs on current page
  smoothScroll.init();


  // Mobile Menu things
  $( ".hamburger" ).click(function() {
    $( this ).toggleClass( "hamburger--close" );
    $( "body" ).toggleClass( "menu-active" );
  });

  $( ".nav-link" ).click(function() {
        $( ".hamburger" ).removeClass( "hamburger--close" );
        $( "body" ).removeClass( "menu-active" );
  });



//Vertical Dot Navigation
  var parPosition = [];
  $('.par').each(function() {
      parPosition.push($(this).offset().top);
  });
  $('.vNav ul li a').click(function() {
      $('.vNav ul li a').removeClass('active');
      $(this).addClass('active');
  });

  $(document).scroll(function() {
      var position = $(document).scrollTop(),
          index;
      for (var i = 0; i < parPosition.length; i++) {
          if (position <= parPosition[i]) {
              index = i;
              break;
          }
      }
      $('.vNav ul li a').removeClass('active');
      $('.vNav ul li a:eq(' + index + ')').addClass(
          'active');
  });
  $('.vNav ul li a').click(function() {
      $('.vNav ul li a').removeClass('active');
      $(this).addClass('active');
  });

    //hide body while everything loads with inline style on body tag
    //once loaded, make body visible
    // another possible solution: http://stackoverflow.com/questions/9550760/hide-page-until-everything-is-loaded-advanced
    jQuery(window).load(function() {
      document.getElementsByTagName("body")[0].style.visibility = "visible";

      // if site is returning from wufoo form, add this to body class. special slug is added with a ?
      var pathname = window.location.href; 
      $("body").addClass( pathname );
    });

    $( ".close-confirmation" ).click(function() {
      $( ".submission-confirmation" ).toggleClass( "confirmation-close" );
    });


}); // End jQuery hack from top




