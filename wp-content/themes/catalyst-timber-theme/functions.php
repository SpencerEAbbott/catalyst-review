<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		parent::__construct();
	}

	function register_post_types() {
		//this is where you can register custom post types
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies
	}

	function add_to_context( $context ) {
		$context['foo'] = 'bar';
		$context['stuff'] = 'I am a value set in your functions.php file';
		$context['notes'] = 'These values are available everytime you call Timber::get_context();';
		$context['menu'] = new TimberMenu();
		$context['site'] = $this;
		return $context;
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own fuctions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter( 'myfoo', new Twig_Filter_Function( 'myfoo' ) );
		return $twig;
	}

}

new StarterSite();

function myfoo( $text ) {
	$text .= ' bar!';
	return $text;
}



/*-----------------------------------------------------------------------------------*/
/*  Stylesheets
/*-----------------------------------------------------------------------------------*/

function wpt_theme_styles() {
  wp_enqueue_style( 'main_css', get_template_directory_uri() . '/assets/stylesheets/application.css?ver=2016.04.12.1' );
}

add_action( 'wp_enqueue_scripts', 'wpt_theme_styles' );


/*-----------------------------------------------------------------------------------*/
/*  Scripts
/*-----------------------------------------------------------------------------------*/


function wpt_theme_js() {

	wp_enqueue_script(
		'jquery'
	);

  wp_enqueue_script(
    'vendor_libraries',
    get_stylesheet_directory_uri() . '/assets/javascript/vendor.min.js?ver=2015.11..1',
    ''
  );
  wp_enqueue_script(
    'main_js',
    get_stylesheet_directory_uri() . '/assets/javascript/application.min.js?ver=2015.11..1',
    array( 'jquery', 'vendor_libraries' )
  );
}

add_action( 'wp_enqueue_scripts', 'wpt_theme_js' );





/*-----------------------------------------------------------------------------------*/
/*  Create a simple sub options pages
/*-----------------------------------------------------------------------------------*/

if( function_exists('acf_add_options_sub_page') )
{
    acf_add_options_sub_page( 'Header' );
    acf_add_options_sub_page( 'Homepage' );
    acf_add_options_sub_page( 'Footer' );
}


/*-----------------------------------------------------------------------------------*/
/*  Create an advanced sub pages that sits under the General options menu.
/*-----------------------------------------------------------------------------------*/

if( function_exists('acf_add_options_sub_page') )
{

    acf_add_options_sub_page(array(
        'title' => 'Header',
        'parent' => 'options-general.php',
        'capability' => 'manage_options'
    ));

    acf_add_options_sub_page(array(
        'title' => 'Homepage',
        'parent' => 'options-general.php',
        'capability' => 'manage_options'
    ));

    acf_add_options_sub_page(array(
        'title' => 'Footer',
        'parent' => 'options-general.php',
        'capability' => 'manage_options'
    ));

}



/* Options for ACF ---- site wide with timber */
add_filter( 'timber_context', 'mytheme_timber_context'  );

function mytheme_timber_context( $context ) {
    $context['option'] = get_fields('option');
    return $context;
}
