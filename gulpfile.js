// http://ilikekillnerds.com/2014/07/how-to-basic-tasks-in-gulp-js/


// Include gulp and plugins
var gulp         = require('gulp'),
    browserSync  = require('browser-sync').create(),
    sass         = require('gulp-sass'),
    rename       = require("gulp-rename"),
    uglify       = require('gulp-uglify'),
    uncss        = require('gulp-uncss'),
    notify       = require("gulp-notify"),
    plumber      = require('gulp-plumber'),

    // Makes getting around easier on varying degrees
    THEME        = 'wp-content/themes/catalyst-timber-theme'



// Static Server + watching scss/html files
gulp.task('serve', function() {
    browserSync.init({
        baseDir: "./"
    });
    gulp.watch(THEME + "/assets/stylesheets/application.css", ['sass']);
    gulp.watch(THEME + "/*.php").on('change', browserSync.reload);
    gulp.watch(THEME + "/**/*.php").on('change', browserSync.reload);
    gulp.watch(THEME + "/templates/*.twig").on('change', browserSync.reload);
    gulp.watch(THEME + "/assets/javascript/application.min.js").on('change', browserSync.reload);

});



gulp.task('sass', function() {
    return gulp.src(THEME + '/assets/stylesheets/*.scss')
        .pipe(plumber())
        .pipe(
          sass({
            outputStyle: 'expanded',
            debugInfo: true,
            lineNumbers: true,
            errLogToConsole: true,
              onSuccess: function(){
                notify().write({ message: "SCSS Compiled successfully! " });
              },
              onError: function(err) {
                gutil.beep();
                notify().write({ message: "Whoops.... SCSS error " });
              }
            }
          )
        )
        .pipe(plumber.stop())
        .pipe(gulp.dest(THEME + '/assets/stylesheets'))
        .pipe(browserSync.stream());

});



// Uglify and Rename our Application JS
// Still Need to set this up for our vender folder as well

gulp.task('compress', function() {
  return gulp.src(THEME + '/assets/javascript/application.js')
    .pipe(plumber())
    .pipe(uglify())
    .pipe(rename(function (path) {
      path.dirname += "/javascript";
      path.basename += ".min";
      path.extname = ".js"
    }))
    .pipe(gulp.dest(THEME + '/assets/'))
    .pipe(plumber.stop())
    .pipe(notify({ message: 'Javascript task complete' }));
});


gulp.task('compress_vendor', function() {
  return gulp.src(THEME + '/assets/javascript/vendor/vendor.js')
    .pipe(plumber())
    .pipe(uglify())
    .pipe(rename(function (path) {
      path.dirname += "/javascript";
      path.basename += ".min";
      path.extname = ".js"
    }))
    .pipe(gulp.dest(THEME + '/assets/'))
    .pipe(plumber.stop())
    .pipe(notify({ message: 'Javascript task complete' }));
});



// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch(THEME + '/assets/**/*.scss', ['sass']);
    gulp.watch(THEME + '/assets/javascript/application.js', ['compress']);
});



// Default Task
gulp.task('default', ['sass', 'compress', 'compress_vendor', 'watch', 'serve']);
